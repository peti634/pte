/* global gl */
var INPUTKEYS = {
    KEY_W: 87,
    KEY_A: 65,
    KEY_S: 83,
    KEY_D: 68,
    KEY_0: 0x30,
    KEY_1: 0x31,
    KEY_2: 0x32,
    KEY_3: 0x33,
    KEY_4: 0x34,
    KEY_5: 0x35,
    KEY_Q: 81,
    KEY_T: 84,
    KEY_E: 0x45,
    KEY_SPACE: 32,
    KEY_CTRL: 17,
    KEY_SHIFT: 16,
    LBUTTON: 0,
    MBUTTON: 1,
    RBUTTON: 2
};

var pve;
var DEBUG = true;
var gl_;
var canv;
var tm_canv;
var ctx;

var napfeny = 1.0;

var icontrol;
var Box;
var vilag;
var terrain;
var terrain2;
var kijelol = {x: 0, y: 0};
var voxelkijelol = [0, 0, 0, -1];
var ObjSzerk;
var Range = 10;
var vvilag;

var box;
var plain;
var plain_tc;

var Voxelraypickbox;
var VoxelEpitTipus = 0;
var showvoxel = false;

var ViszonyPont;

var TerepSzerkTipus = 0;
var tatlas;

var kij_pos = [0, 0, 0];
var kij_mer = [1, 1, 1];

function terrainToVoxel() {
    vilag.updateFromTerrain(!showvoxel);
}

function Start() {
    var utvonal_prefix = "tex/";

    canv = $("glcanvas");
    pve = new PokolVoxelEngine();
    var sett = new PokolVoxelEngine_DefaultSettings();
    sett.canvas = canv;

    sett.render_maxrange = 500;
    sett.voxel_napfenyRange = 4;
    sett.voxel_pontfenyMaxRange = 100;
    sett.voxel_blokSsize = 0.25;
    sett.voxel_blokkOldalFeny = [0.78, 0.7, 0.4, 1.0, 0.6, 0.85];
    sett.voxel_terrainLengthXZ = 30;
    sett.blokkTypeList = BlockTypeList;
    sett.blokkCsoportList = CsoportTypeList;
    sett.url_prefix = utvonal_prefix;


    sett.callback_draw = frameTick;

    gl_ = pve.initGL(sett);

    if (gl_) {

        tatlas = pve.getTerrainAtlas();

        $("menu").style.display = "block";

        $("startbut").style.display = "none";


        icontrol = pve.inputControl(Mouse_CB, MouseWheel_CB, Key_CB);
        icontrol.LoadInputKey(INPUTKEYS);
        icontrol.SetPos(0, 4, 0);

        icontrol.SetNezszo(10, 130);



        vvilag = pve.voxelVilag;
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                vvilag.addTerrain(i, j);

            }
        }

        vvilag.update();
        var tolpos = [-vvilag.blocksize / 2, 0, -vvilag.blocksize / 2];
        vvilag.setRelativePos(tolpos);
        ViszonyPont = new pve.geos.Geo_Box([0.2, 0.2, 0.2], [0, 0, 0], [0, 0, 0]);
        pve.geos.addList(ViszonyPont);
        ViszonyPont.yheight = 0;
        ViszonyPont.fadecolor = 0;


        vilag = pve.terrainVilag;//(0.25, 63)
        ////vilag.AddTerrain(0, 0);
        //vilag.AddTerrain(1, 0);

        Voxelraypickbox = new pve.geos.Geo_Box([0.252, 0.252, 0.252], [0, 0, 0], [0, 0, 0]);
        pve.geos.addList(Voxelraypickbox);


        for (var i = 0; i < 5; i++) {
            for (var j = 0; j < 5; j++)
                vilag.AddTerrain(i, j);
        }
        //vilag.AddTerrain(0,0);
        //vilag.AddTerrain(1,0);
        //


        for (var i in CsoportTypeList) {
            var elem = CsoportTypeList[i];
            AddBlockCsoport(elem, utvonal_prefix);
        }



        ObjSzerk = pve.terrainFormer(0.25, 0.05);
        pve.start();
        setInterval("kiirFPS()", 1000);



    }
}

function frameDraw() {

    var usefeny = $("cb_fenye").checked;
    if (usefeny) {
        pve.clear([0.5 * napfeny, 0.5 * napfeny, 0.8 * napfeny, 1.0]);
    } else {
        pve.clear([0.5, 0.5, 0.8, 1.0]);
    }

    pve.updateCamera(icontrol, [mopos.x, mopos.y]);


    if (showvoxel) {

        voxelkijelol = vvilag.rayPick();
        if (voxelkijelol[3] !== -1) {
            voxelkijelol[0] += kij_pos[0];
            voxelkijelol[1] += kij_pos[1];
            voxelkijelol[2] += kij_pos[2];
        }
        if (voxelkijelol[3] !== -1 && VoxelEpitTipus !== -1) {
            var oldal = voxelkijelol[3];
            if (oldal === 1)
                voxelkijelol[1]--;
            if (oldal === 2)
                voxelkijelol[1]++;
            if (oldal === 3)
                voxelkijelol[2]--;
            if (oldal === 4)
                voxelkijelol[2]++;
            if (oldal === 5)
                voxelkijelol[0]--;
            if (oldal === 6)
                voxelkijelol[0]++;


        }
    }



    if (!showvoxel) {
        vilag.Update();


        var objszerk_upd = !icontrol.key(INPUTKEYS.RBUTTON) || TerepSzerkTipus !== 0;

        if (objszerk_upd) {
            kijelol = vilag.RayPick(icontrol.pos, TerepSzerkTipus === 1);
        }
        ObjSzerk.setPos(kijelol.x, kijelol.x);
        if (kijelol.x !== -1) {

            var szerktipus = 0;
            if (document.getElementById("rad_11").checked)
                szerktipus = 1;
            ObjSzerk.UpdateData(Range, szerktipus);
            var HaszNan = document.getElementById("chbox_3").checked;
            var Erzekeny = document.getElementById("ErzekScroll").value / 100;
            ObjSzerk.UpdateHeight(kijelol.x, kijelol.y, vilag, objszerk_upd, HaszNan, Erzekeny, TerepSzerkTipus === 1);



        }

    }

    gl_.enable(gl_.CULL_FACE);


    if (voxelkijelol[3] !== -1 && showvoxel) {

        var kjx = voxelkijelol[0] * vvilag.blocksize - 0.126;
        var kjy = voxelkijelol[1] * vvilag.blocksize - 0.001;
        var kjz = voxelkijelol[2] * vvilag.blocksize - 0.126;
        Voxelraypickbox.setPos([kjx, kjy, kjz]);

        Voxelraypickbox.setHidden(false);

    } else
        Voxelraypickbox.setHidden(true);
        


    gl_.mvPush();

    pve.napfeny = napfeny;
    var usefeny = $("cb_fenye").checked;
    vvilag.Draw(usefeny);



    gl_.mvPop();


    gl_.disable(gl_.CULL_FACE);

    if (!showvoxel) {



        vilag.Draw(false);


        if (kijelol.x !== -1) {
            ObjSzerk.Draw(kijelol, TerepSzerkTipus === 1);
        }



        if (document.getElementById("chbox_1").checked) {

            gl_.blendFunc(gl_.DST_ALPHA, gl_.ONE_MINUS_SRC_ALPHA);
            gl_.enable(gl_.BLEND);
            gl_.disable(gl_.DEPTH_TEST);

            vilag.Draw(true);

            gl_.disable(gl_.BLEND);
            gl_.enable(gl_.DEPTH_TEST);
        }
        if (document.getElementById("rad_3").checked) {
            ViszonyPont.setHidden(false);

        } else
            ViszonyPont.setHidden(true);
    }

}

function KeyTick() {
    icontrol.start();
    var speed = 0.05;
    if (icontrol.key(INPUTKEYS.KEY_SHIFT))
        speed = 0.5;
    if (icontrol.key(INPUTKEYS.KEY_W)) {
        icontrol.MoveCam(icontrol.nezszog[0], icontrol.nezszog[1], speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_S)) {
        icontrol.MoveCam(icontrol.nezszog[0], icontrol.nezszog[1], -speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_D)) {
        icontrol.MoveCam(0, icontrol.nezszog[1] + 90, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_A)) {
        icontrol.MoveCam(0, icontrol.nezszog[1] - 90, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_SPACE)) {
        icontrol.MoveCam(-90, 0, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_CTRL)) {
        icontrol.MoveCam(90, 0, speed);
    }
    if (icontrol.key(INPUTKEYS.MBUTTON)) {
        if (document.getElementById("rad_3").checked && kijelol.x !== -1) {

            ViszonyPont.yheight = vilag.GetHeight(kijelol.x, kijelol.y);

            ViszonyPont.setPos ([kijelol.x * 0.25 - 0.1, ViszonyPont.yheight - 0.1, kijelol.y * 0.25 - 0.1]);

        }
    }
    if (document.getElementById("rad_3").checked) {
        if ($("help_viszonypont").style.display === "none")
            $("help_viszonypont").style.display = "";

    } else {
        if ($("help_viszonypont").style.display === "")
            $("help_viszonypont").style.display = "none";
    }
    icontrol.end();
}

var fps = 0;
var frametime;
var lowframetime;

function frameTick() {
    var akttime = performance.now();
    if (lowframetime === 0 || lowframetime < (akttime - frametime))
        lowframetime = akttime - frametime;
    frametime = akttime;

    fps++;


    KeyTick();
    frameDraw();


    ViszonyPont.fadecolor++;
    ViszonyPont.fadecolor %= 100;


    var color = ViszonyPont.fadecolor / 50;
    color = (color > 1) ? (2 - color) : (color);
    ViszonyPont.setColor( [color, color, color]);
    Voxelraypickbox.setColor( [color, color, color]);

}
function kiirFPS() {
    var perfstr = (((lowframetime * 1000) | 0) / 1000);
    $("fps").innerHTML = "FPS: " + fps + "<BR>usec:" + perfstr;
    lowframetime = 0;
    fps = 0;
    osszdraw = 0;
}
