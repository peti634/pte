/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function $(id) {
    return document.getElementById(id);
}

//window.onbeforeunload = function () {return "Bzitosan bezárod a szerkesztőt?";};

function CreateScrollbar(kiir, nev, value, min, max, size, func) {
    var str = kiir + '<span style="padding-left:20px;" id="kiir_' + nev + '">' + value + '</span><BR>';
    str += '<div><button style="vertical-align: super; width: 18px; height: 20px; padding: 0 0 0 0;" onclick="$(\'' + nev + '\').stepDown();$(\'' + nev + '\').oninput();">&lt;</button>';
    var atrb = 'min="' + min + '" max="' + max + '" value="' + value + '" style="width: ' + (size - 40) + 'px; height: 20px;" id="' + nev + '"';
    var mwheel = 'onmousewheel="if (((event.wheelDelta) ? (event.wheelDelta) : (-event.detail)) > 0) $(\'' + nev + '\').stepUp(); else $(\'' + nev + '\').stepDown(); $(\'' + nev + '\').oninput();"';
    str += '<input  type="range" ' + atrb + ' step="1"  oninput="$(\'kiir_' + nev + '\').innerHTML = $(\'' + nev + '\').value;' + func + '"  ' + mwheel + '/>';
    str += '<button style="vertical-align: super; width: 18px; height: 20px; padding: 0 0 0 0;" onclick="$(\'' + nev + '\').stepUp();$(\'' + nev + '\').oninput();">&gt;</button></div>';
    document.write(str);
}

function VoxelClick() {
    for (var x = 0; x < kij_mer[0]; x++) {
        for (var y = 0; y < kij_mer[1]; y++) {
            for (var z = 0; z < kij_mer[2]; z++) {
                var vposkijx = voxelkijelol[0] + x;
                var vposkijy = voxelkijelol[1] + y;
                var vposkijz = voxelkijelol[2] + z;
                if (VoxelEpitTipus === -1) {
                    vvilag.delElem(vposkijx, vposkijy, vposkijz);
                }
                if (VoxelEpitTipus > 0) {
                    //
                    var r = parseInt($("sett_feny_r").value);
                    var g = parseInt($("sett_feny_g").value);
                    var b = parseInt($("sett_feny_b").value);
                    var color = r + (g << 8) + (b << 16);
                    var range = parseInt($("sett_feny_range").value);

                    var VoxelData = new Uint32Array([color, range]);
                    vvilag.addElem(vposkijx, vposkijy, vposkijz, VoxelEpitTipus, VoxelData);
                }
            }
        }
    }

    vvilag.update();
}


function MouseWheel_CB(e) {
    if (e > 0)
        Range++;
    if (e < 0)
        Range--;
    if (Range < 0)
        Range = 0;

}


var mopos = {x: 0, y: 0};

function Mouse_CB(e, movm, mpos) {
    if (icontrol.key(INPUTKEYS.RBUTTON) && !showvoxel) {



        if (kijelol.x === -1)
            return;
        var tipus = 0;
        if (document.getElementById("rad_1").checked)
            tipus = 1;
        if (document.getElementById("rad_2").checked)
            tipus = 2;
        if (document.getElementById("rad_3").checked)
            tipus = 3;
        if (document.getElementById("rad_4").checked)
            tipus = 4;
        if (document.getElementById("rad_5").checked) {
            if (document.getElementById("chbox_2").checked)
                tipus = 6;
            else
                tipus = 5;
        }
        if (TerepSzerkTipus === 0) {
            ObjSzerk.FormingTerrain(movm.y, kijelol, ViszonyPont.yheight, tipus, vilag);
        }
        if (TerepSzerkTipus === 1) {
            ObjSzerk.Paint(kijelol, $("tipuslist").selectedIndex, vilag);

        }
    }
    if (GetPointerLock()) {
        if (icontrol.key(INPUTKEYS.RBUTTON) && TerepSzerkTipus === 0)
            return;
        icontrol.nezszog[0] += movm.y / 10.0;
        icontrol.nezszog[1] += movm.x / 10.0;


        mopos.x = canv.width >> 1;
        mopos.y = canv.height >> 1;

    } else {
        mopos = mpos;
    }
}


function Key_CB(key, up, dbclick) {
    if (up)
        return;
    if (key === INPUTKEYS.LBUTTON) {
        icontrol.MouseLock();
        return;
    }
    switch (key) {
        case INPUTKEYS.KEY_0:
            if ($("rad_10").checked)
                $("rad_11").checked = true;
            else
                $("rad_10").checked = true;
            break;
        case INPUTKEYS.KEY_1:
            $("rad_1").checked = true;
            break;
        case INPUTKEYS.KEY_2:
            $("rad_2").checked = true;
            break;
        case INPUTKEYS.KEY_3:
            $("rad_3").checked = true;
            break;
        case INPUTKEYS.KEY_4:
            $("rad_4").checked = true;
            break;
        case INPUTKEYS.KEY_5:
            $("rad_5").checked = true;
            break;
        case INPUTKEYS.KEY_Q:
            $("chbox_1").checked = !$("chbox_1").checked;
            break;
        case INPUTKEYS.KEY_T:
            $("chbox_3").checked = !$("chbox_3").checked;
            break;
        case INPUTKEYS.RBUTTON:
            if (showvoxel) {
                if (voxelkijelol[3] !== -1) {
                    VoxelClick();
                }

            } else {
                if (TerepSzerkTipus === 1) {
                    ObjSzerk.Paint(kijelol, $("tipuslist").selectedIndex, vilag);

                }
            }
            break;
    }
    if (icontrol.key(INPUTKEYS.KEY_CTRL) && icontrol.key(INPUTKEYS.KEY_S)) {
        return true;
    }
}


var showdiv_name = "";
function ShowSettDiv(name) {
    if (showdiv_name !== "")
        $(showdiv_name).style.display = "none";
    if (name !== "")
        $(name).style.display = "";
    showdiv_name = name;
}

function TreeViewClick(e) {
    var target = e.target;

    if (!target || !target.getAttribute("data-type"))
        return;
    var type = target.getAttribute("data-type");
    var data_id = target.getAttribute("data-id");

    if (type === "block") {
        VoxelEpitTipus = parseInt(data_id);
        var tipelem = BlockTypeList[VoxelEpitTipus];
        if (tipelem !== undefined) {
            if (tipelem.tul.pontfeny_rgb === 1) {
                ShowSettDiv("sett_feny");
            } else {
                ShowSettDiv("");
            }
        }

        $("SPANepitkiir").innerHTML = target.innerHTML + " #" + target.getAttribute("data-id");

    } else if (type === "kij") {
        ShowSettDiv("sett_kij_mer");
    }



}

function AddBlockCsoport(elem, url_prefix) {
    var li_elem = document.createElement("LI");
    li_elem.innerHTML = '<input type="checkbox" id="item-0-' + elem.id + '" /><label for="item-0-' + elem.id + '" >' + elem.name + '</label>';
    var elemstr = "";

    for (var i = 0; i < elem.elemek.length; i++) {
        var cselemid = elem.elemek[i];
        var cselem = BlockTypeList[cselemid];
        elemstr += '<li><a href="javascript:void(0);" data-type="block" data-id="' + cselemid + '"><div class="block_kep"></div>' + cselem.name + '</a></li>';
        if (cselem.url !== "") {
            var src = url_prefix + cselem.url;

            if (elem.tul.obj3d === 0) {
                var option = document.createElement("OPTION");
                option.innerHTML = cselem.name;

                option.id = "text" + src;
                $("tipuslist").appendChild(option);
                var img = new Image();
                img.src = src;
                option.img = img;

            }
        }
    }


    li_elem.innerHTML += '<ul>' + elemstr + '</ul>';
    $("ULblockok").appendChild(li_elem);
}

function scrollNapfeny(e) {
    napfeny = e.value / e.max;

}
function nmdment() {
    var opt = new PokolVoxelEngine_DefaultsaveMap_options();
    opt.saveTerrainData = 2;
    opt.saveVoxelData = true;

    pve.saveMap("mnd.zip", opt);
}

function pte_ment() {
    var opt = new PokolVoxelEngine_DefaultsaveMap_options();
    opt.saveTerrainData = 1;
    opt.saveVoxelData = true;
    pve.saveMap("pte.zip", opt);
}
function pte_betolt_cb(data) {
    if (data.error !== 0) {
        alert("Hiba a betöltéskor: " + data.error);
        return false;
    }
    if (data.terrainvilag === undefined) {
        alert("Terrain adatok hiányoznak!");
        return false;
    }
    if (data.newvoxelvilag === undefined) {
        alert("Voxel adatok hiányoznak!");
        return false;
    }
    vilag = data.terrainvilag;
    vvilag = data.newvoxelvilag;

    alert("Betöltés sikeres, adatok alkalmazása!");
    return true;
}
function pte_betolt() {
    pve.loadMap(pte_betolt_cb);
}
var texc;
function texvalt(e) {
    var sindex = $("tipuslist").selectedIndex;
    if (sindex === -1)
        return;
    var img = $("tipuslist")[sindex].img;
    if (img.complete) {
        if (texc === undefined) {
            texc = $("texcanvas").getContext("2d");
        }
        texc.drawImage(img, 0, 0, 150, 150);
    }
}
function terepszerkvalt() {
    $("terepformalasdiv").style.display = "none";
    $("terepfestesdiv").style.display = "none";
    $('terraindiv').style.display = "none";
    $("voxelszerkdiv").style.display = "none";

    if (showvoxel === true && $("voxelszerk").selected === false) {
        showvoxel = false;
        ShowSettDiv("");
        terrainToVoxel();
    }
    if ($("terepformalas").selected) {
        TerepSzerkTipus = 0;
        $("terepformalasdiv").style.display = "inline";
        $('terraindiv').style.display = "block";

    } else if ($("terepfestes").selected) {
        TerepSzerkTipus = 1;
        $("terepfestesdiv").style.display = "inline";
        $('terraindiv').style.display = "block";
    } else {
        if (showvoxel === false) {
            showvoxel = true;
            terrainToVoxel();
            $('voxelszerkdiv').style.display = 'block';
        }
    }
}


function update_kijelol(resize) {
    if (resize) {
        var merx = parseInt($("sett_kijmx").value);
        var mery = parseInt($("sett_kijmy").value);
        var merz = parseInt($("sett_kijmz").value);
        kij_mer = [merx, mery, merz];
        Voxelraypickbox.resize(0.252 * merx, 0.252 * mery, 0.252 * merz);

    }




    var posx = parseInt($("sett_kijpx").value);
    var posy = parseInt($("sett_kijpy").value);
    var posz = parseInt($("sett_kijpz").value);
    kij_pos = [posx, posy, posz];
}