function BlockType(id, name, url, objname, csoport, csindex, leiras, tul) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.objname = objname;
    this.csoport = csoport;
    this.csoportindex = csindex;
    this.leiras = leiras;
    this.tul = tul;
    this.taxatlas_index = -1;//csak textúra atlas használatakor, atlas helyét mutatja majd meg
}
function CsoportType(id, name) {
    this.id = id;
    this.name = name;
    this.elemek = [];
    this.tul = {
        fenyblockade: 0,
        vizalpha: 0,
        texatlas: 0,
        obj3d: 0,
        pontfeny_rgb: 0,
        szabadfeny: 0,
    };
}


var BlockTypeList = [];
var CsoportTypeList = [];

function NewCsoportType(id, name, tullist) {
    CsoportTypeList[id] = new CsoportType(id, name);
    for (var i = 0; i < tullist.length; i++) {
        CsoportTypeList[id].tul[tullist[i]] = 1;
    }
}
function AddCsoportElem(csid, bid) {
    var cselem = CsoportTypeList[csid];
    return cselem.elemek.push(bid) - 1;

}
function NewBlockType(id, name, url, objname, csoport, leiras) {
    var ret = AddCsoportElem(csoport, id);
    BlockTypeList[id] = new BlockType(id, name, url, objname, csoport, ret, leiras, CsoportTypeList[csoport].tul);

}

NewCsoportType(1, "alapterrain", ["texatlas","fenyblockade"]);
NewCsoportType(2, "víz", ["vizalpha", "texatlas","szabadfeny"]);
NewCsoportType(3, "láva", ["vizalpha", "texatlas","szabadfeny"]);
NewCsoportType(10, "fény", ["obj3d","pontfeny_rgb","szabadfeny"]);

NewBlockType(1, "terep fű","ground_grass_gen_05.png", "", 1, "Fű");
NewBlockType(2, "kő","rock_3.png", "", 1, "Kő");
NewBlockType(3, "Kikövezett út","_paving2.png", "", 1, "Kikövezett út");
NewBlockType(4, "Csempe","_paving6.png", "", 1, "???");
NewBlockType(5, "Fa léc","_planks.png", "", 1, "???");
NewBlockType(6, "kő 2","_rock6.png", "", 1, "???");
NewBlockType(7, "kő fal","_stonewall4.png", "", 1, "???");
NewBlockType(8, "kő fal 2","_stonewall5.png", "", 1, "???");
NewBlockType(9, "kő fal 3","_stonewall6.png", "", 1, "???");
NewBlockType(10, "kő fal 4","_stonewall8.png", "", 1, "???");
NewBlockType(11, "Fa léc 2","_woodfloor1.png", "", 1, "???");
NewBlockType(12, "Fa törsz","_vegetation_tree_bark_40.png", "", 1, "???");

NewBlockType(13, "víz","water.png", "", 2, "???");

NewBlockType(14, "láva","lava.png", "", 3, "???");

NewBlockType(100, "RGB Pont fényforrás","", "Geo_Feny", 10, "???");
